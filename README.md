# Hugo Micro-typographie

Un modèle de page partiel (_partial_) pour un traitement automatisé de la micro typographie française dans les pages générées par [Hugo](https://gohugo.io/).

A noter que ce script ne règle pas _tous_ les problèmes éventuels de micro typographie, il suppose donc que les textes au format Markdown traités sont déjà plutôt « propres », en particulier en ce qui concerne un usage correcte des guillemets et de l’apostrophe typographique.

### Corrections

Les espaces typographiques :

- Insécable ou fine insécable devant les signes de ponctuation qui le nécessitent (`:`,`;`,`?`,`!`).
- Fine insécable pour les guillemets français (`«` et `»`), mais pas pour les guillemets anglais (`“` et `”`).
- Insécable entre le montant et les symbole de monnaies (`€`, `$`, `£`).
- Insécable ou fine insécable entre des chiffres et certaines unités (`km`, `%` par exemple).

Les caractères typographiques :

- Le caractère caractère `x` est remplacé par un signe `×` (`&time;`) dans les multiplications.
- Le signe degré (`°`) est remplacé par un `o` en exposant (par exemple n<sup>o</sup>3).
- Les ordinnaux (1er, 2e, 3ème, etc.) sont remplacés par 1<sup>er</sup>, 2<sup>e</sup> ou 3<sup>ème</sup>.

Ne sont par contre _pas_ traités :

1. Les doubles _quotes_ (`"`) à remplacer par des guillemets à la française (`«` et `»`).
2. Les simples _quotes_ (`'`) à remplacer par une apostrophe typographique (`’`).
3. L’ajout d’une espace insécable après (ou avant, suivant les cas) un tiret long ou moyen.
4. Le remplacement des ponctuations spéciales (`?!`, `!?`, `!!`) par le signe approprié (`&#8264;`, `&#8265;`, `&#8252;`).
5. Les trois points consécutifs (`...`) à remplacer par une véritable éllipse typographique (`…`).
6. La génération de caractères spéciaux comme ©, ®, ™ ou ℗ en utilisant une lettre entourée de parenthèses.

Remarque : une bonne partie des éléments ci-dessus (apostrophes, élipses, etc.) sont en fait corrigés _par défaut_ par les fonctions typographiques intégrées de Goldmark, le _parseur_ Markdown de Hugo.

### Installation

Il suffit de copier le fichier `micro-typo.html` dans le sous-répertoire `partials/` du répertoire `layouts/` qui est à la _racine_ de votre site Hugo. Si nécessaire créer ces deux dossiers.

Remarque : dans une version _ultérieure_ il devrait peut-être être possible de l'installer directement comme module. Je ne suis par ailleurs pas vraiment certains que cela apporte quelque chose. Ceci n’est pas une limitation, mais un choix.

### Usage

Dans les modèles de pages Hugo (_templates_) où l’on souhaite appliquer la correction de la micro typographie française, il de remplacer remplacer l’appel générique `{{ . }}` au contenu par une inclusion de notre fichier :

```
{{ partial "micro-typo" .Content }}
```

Le traitement de la micro typographie peut en fait s’appliquer à n’importe quel contenu de notre page. Par exemple, pour corriger aussi le titre, il suffit de remplacer l’appel `{{ .Title }}` par :

```
{{ partial "micro-typo" .Title }}
```

### Options

Il existe un paramètre de configuration pour appliquer une _accessibilité_ au point médian par l’utilisation de `aria-hidden` (voir les explications sur cette page de la [documentation MDM](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-hidden)). 

Par défaut cette option est inactive (`false`). Pour activer cette option ajouter `pointMedian = true` dans le fichier `config.toml` du site.

### Crédits

L’essentiel du code utilisé ici est repris depuis le module [Hugo Microtypo](https://github.com/jygastaud/hugo-microtypo) développé par Jean-Yves Gastaud ([@jygastaud](https://github.com/jygastaud)) et Frank Taillandier ([@DirtyF](https://github.com/DirtyF)), qui s’inspire lui-même du plugin [Jekyll Microtypo](https://github.com/borisschapira/jekyll-microtypo) par Boris Schapira ([@borisschapira](https://github.com/borisschapira)).

Ce que j’ai modifié :

- Le nom du _partial_ : J’utilise `micro-typo` plutôt que `content` qui me semble préter à confusion à l’usage.
- Ajout du traitement des guillemets français par un rechercher/remplacer sauvage, pas très élégant mais fonctionnel.
- Commentaires dans le code en français... puisque c’est de micro typographie française dont il est question.

### Licence

Rien ;-)